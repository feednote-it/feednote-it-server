import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Article from './views/Article.vue'
import Channel from './views/Channel.vue'
import Explore from './views/Explore.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: "/",
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/login', name: 'login', component: Login },
    { path: '/register', name: 'register', component: Register },
    { path: '/article/:id', name: 'article', component: Article },
    { path: '/channel/:id', name: 'channel', component: Channel },
    { path: '/explore', name: 'explore', component: Explore }
  ]
})
