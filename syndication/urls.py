from django.urls import path, include
from rest_framework.routers import DefaultRouter

from syndication import views

router = DefaultRouter()
router.register(r'articles', views.ArticleViewSet, base_name='article')
router.register(r'channels', views.ChannelViewSet, base_name='channel')
router.register(r'subscriptions', views.SubscriptionViewSet,
                base_name='subscription')

urlpatterns = [
    path('', include(router.urls)),
]
