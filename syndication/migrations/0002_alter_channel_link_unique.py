# Generated by Django 2.1.1 on 2018-09-13 01:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('syndication', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='link',
            field=models.URLField(unique=True),
        ),
    ]
