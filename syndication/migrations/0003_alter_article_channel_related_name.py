# Generated by Django 2.1.1 on 2018-10-14 23:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('syndication', '0002_alter_channel_link_unique'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='channel',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='articles', to='syndication.Channel'),
        ),
    ]
