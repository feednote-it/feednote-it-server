from __future__ import absolute_import, unicode_literals

from django.db.transaction import on_commit
from rest_framework import mixins, permissions, viewsets

from syndication import serializers
from syndication.models import Article, Channel, Subscription
from syndication.tasks import obtain_articles, subscribe_to_channel


# Create your views here.
class ArticleViewSet(viewsets.ReadOnlyModelViewSet):
    """
    retrieve:
    Return the given article.

    list:
    Return a list of all the existing articles.
    """
    queryset = Article.objects.all()
    serializer_class = serializers.ArticleSerializer
    permission_classes = (permissions.IsAuthenticated,)


class ChannelViewSet(
        mixins.CreateModelMixin,
        mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    """
    create:
    Create a new channel instance with its creator.

    retrieve:
    Return the given channel.

    list:
    Return a list of all the existing channels.
    """
    queryset = Channel.objects.all()
    serializer_class = serializers.ChannelSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @staticmethod
    def _tasks(channel, user):
        obtain_articles.delay(channel.pk)
        subscribe_to_channel.delay(channel.pk, user.pk)

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)
        on_commit(lambda: self._tasks(serializer.instance, self.request.user))


class SubscriptionViewSet(
        mixins.CreateModelMixin,
        mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        mixins.DestroyModelMixin,
        viewsets.GenericViewSet):
    """
    create:
    Create a new subscription.

    retrieve:
    Return a subscription instance.

    destroy:
    Remove the given subscription.

    list:
    Return a list of all the user's subscriptions.
    """
    serializer_class = serializers.SubscriptionSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(subscriber=self.request.user)

    def get_queryset(self):
        queryset = None
        if self.action == 'list':
            queryset = Subscription.objects.filter(
                subscriber=self.request.user)
        else:
            queryset = Subscription.objects.all()
        return queryset
