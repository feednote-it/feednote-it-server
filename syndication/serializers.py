from rest_framework import serializers

from syndication.models import Channel, Article, Subscription


class ChannelSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    created_by = serializers.ReadOnlyField(source='created_by.username')
    articles = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name='article-detail')
    subscribers = serializers.ReadOnlyField(source='get_total_subscribers')

    class Meta:
        model = Channel
        fields = ('id', 'url', 'created', 'updated', 'published',
                  'title', 'description', 'author', 'publisher',
                  'generator', 'link', 'language', 'license',
                  'rights', 'created_by', 'subscribers', 'articles')


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    editable = False

    class Meta:
        model = Article
        fields = '__all__'


class SubscriptionSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Subscription
        fields = ('subscriber', 'channel', 'id', 'url', 'created')
        extra_kwargs = {
            'created': {'read_only': True}
        }
