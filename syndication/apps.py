from django.apps import AppConfig


class SyndicationConfig(AppConfig):
    name = 'syndication'
