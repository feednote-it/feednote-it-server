from rest_framework import permissions


class IsAuthorOrIsPublic(permissions.BasePermission):
    """
    Object-level permission to only allow author of an object to edit it.
    Assumes the model instance has an `author` attribute.
    """
    def has_object_permission(self, request, view, note):
        # Read permissions are allowed to any request.
        # So we'll always allow GET, HEAD, OPTIONS and if the note is public.
        if request.method in permissions.SAFE_METHODS and note.is_public:
            return True
        # Write permissions are only allowed to the author of the note.
        return note.author == request.user
