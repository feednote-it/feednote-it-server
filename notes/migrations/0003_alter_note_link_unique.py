# Generated by Django 2.1.1 on 2018-11-05 01:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0002_alter_note_title_null'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='link',
            field=models.URLField(null=True, unique=True),
        ),
    ]
