from rest_framework import permissions, status, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from notes.models import Note
from notes.permissions import IsAuthorOrIsPublic
from notes.serializers import NoteSerializer


@api_view(['GET'])
def retrieve_public_note(request, link):
    """
    Let you get the given public note.
    """
    try:
        note = Note.objects.get(link=link, is_public=True)
        serializer = NoteSerializer(note, context={'request': request})
        return Response(serializer.data)
    except Note.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)



# Create your views here.
class NoteViewSet(viewsets.ModelViewSet):
    """
    create:
    Let you create a note associated to an article.

    retrieve:
    Let you get the given note.

    update:
    Let you update a note content.

    destroy:
    Let you destroy a specific note.

    list:
    Return a list of all the existing notes of the user.
    """
    queryset = Note.objects.all()
    serializer_class = NoteSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_permissions(self):
        if self.action == 'retrieve':
            permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                                  IsAuthorOrIsPublic]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    def get_queryset(self):
        queryset = None
        if self.action == 'list':
            queryset = Note.objects.filter(author=self.request.user)
        else:
            queryset = Note.objects.all()
        return queryset
