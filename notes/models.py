from django.db import models


# Create your models here.
class Note(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=256, null=True)
    content = models.TextField()
    link = models.CharField(max_length=100, unique=True, null=True)
    is_public = models.BooleanField(default=False)
    author = models.ForeignKey(
        'auth.User', related_name='notes', on_delete=models.CASCADE, null=True)
    article = models.ForeignKey(
        'syndication.Article', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('created',)
