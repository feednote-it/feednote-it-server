from rest_framework import serializers

from notes.models import Note


class NoteSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Note
        fields = ('created', 'updated', 'title', 'content',
                  'link', 'is_public', 'author', 'article')
        extra_kwargs = {
            'created': {'read_only': True},
            'updated': {'read_only': True},
        }
        depth = 1
