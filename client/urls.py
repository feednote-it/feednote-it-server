from django.urls import path, re_path

from client.views import index_view

urlpatterns = [
    path('', index_view, name='index'),
    re_path(r'^(?:.*)/?$', index_view),
]
